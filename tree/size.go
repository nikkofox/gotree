package tree

import (
	"fmt"
	"github.com/dustin/go-humanize"
)

type Size struct {
	Config *Config
	Enable bool
}

func (s *Size) calc(size int64) string {
	switch {
	case s.Config.HumanSiSize:
		return humanize.Bytes(uint64(size))
	case s.Config.HumanSize:
		return humanize.IBytes(uint64(size))
	default:
		return fmt.Sprintf("%vb", size)
	}
}

func (s *Size) format(leftBracket, rightBracket string, size int64) string {
	return fmt.Sprintf(" %v%v%v", leftBracket, s.calc(size), rightBracket)
}

func IsEnabled(config *Config) bool {
	return config.BytesSize || config.HumanSize || config.HumanSiSize
}
