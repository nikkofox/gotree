package tree

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/fatih/color"
)

const (
	onlyTab    = "    "
	clearPipe  = "│   "
	middlePipe = "├── "
	bottomPipe = "└── "
)

type Tree struct {
	Output io.Writer
	Config *Config
	Size   *Size
}

func isExecAll(mode os.FileMode) bool {
	return mode&0111 == 0111
}

func (t *Tree) readDir(root string) ([]string, error) {
	var files []string
	fileInfo, err := os.ReadDir(root)
	if err != nil {
		return files, err
	}

	for _, f := range fileInfo {
		if t.Config.OnlyDir && !f.IsDir() {
			continue
		}
		if !t.Config.AllFiles && strings.HasPrefix(f.Name(), ".") {
			continue
		}

		files = append(files, f.Name())
	}
	return files, nil
}

func (t *Tree) draw(path, indent string) error {
	listDir, err := t.readDir(path)
	if err != nil {
		return err
	}

	for i, fn := range listDir {
		filePath := filepath.Join(path, fn)
		currentPipe := middlePipe
		nextIndent := clearPipe
		if len(listDir) == i+1 {
			currentPipe = bottomPipe
			nextIndent = onlyTab
		}

		fi, err := os.Stat(filePath)
		if err != nil {
			return err
		}

		if fi.IsDir() {
			fmt.Fprintf(t.Output, "%v%v%v\n", indent, currentPipe, color.BlueString("%v", fn))

			err = t.draw(filePath, indent+nextIndent)
			if err != nil {
				return err
			}
		} else {
			size := ""
			if t.Size.Enable {
				size = t.Size.format("(", ")", fi.Size())
			}
			if isExecAll(fi.Mode()) {
				fn = color.GreenString(fn)
			}
			fmt.Fprintf(t.Output, "%v%v%v%v\n", indent, currentPipe, fn, size)
		}
	}

	return nil
}

func (t *Tree) DirTree(path string) error {
	fileInfo, err := os.Stat(path)
	if err != nil {
		return err
	}
	if !fileInfo.IsDir() {
		return fmt.Errorf("%v is not a directory", path)
	}

	if t.Config.WithoutColor {
		color.NoColor = true
	}

	fmt.Fprintln(t.Output, color.BlueString("."))
	return t.draw(path, "")
}
