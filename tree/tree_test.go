package tree

import (
	"bytes"
	"testing"
)

const testFullResult = `.
├── project
│   ├── file.txt
│   └── gopher.png
├── static
│   ├── a_lorem
│   │   ├── dolor.txt
│   │   ├── gopher.png
│   │   └── ipsum
│   │       └── gopher.png
│   ├── css
│   │   └── body.css
│   ├── empty.txt
│   ├── html
│   │   └── index.html
│   ├── js
│   │   └── site.js
│   └── z_lorem
│       ├── dolor.txt
│       ├── gopher.png
│       └── ipsum
│           └── gopher.png
├── zline
│   ├── empty.txt
│   └── lorem
│       ├── dolor.txt
│       ├── gopher.png
│       └── ipsum
│           └── gopher.png
└── zzfile.txt
`

func TestTreeFull(t *testing.T) {
	out := new(bytes.Buffer)
	config := Config{
		OnlyDir:      false,
		AllFiles:     false,
		BytesSize:    false,
		HumanSize:    false,
		HumanSiSize:  false,
		WithoutColor: true,
	}

	tr := Tree{
		Output: out,
		Config: &config,
		Size:   &Size{Config: &config, Enable: IsEnabled(&config)},
	}

	err := tr.DirTree("../testdata")
	if err != nil {
		t.Errorf("test for OK Failed - error")
	}
	result := out.String()

	if result != testFullResult {
		t.Errorf("test for OK Failed - results not match\nGot:\n%v\nExpected:\n%v", result, testFullResult)
	}
}

const testDirResult = `.
├── project
├── static
│   ├── a_lorem
│   │   └── ipsum
│   ├── css
│   ├── html
│   ├── js
│   └── z_lorem
│       └── ipsum
└── zline
    └── lorem
        └── ipsum
`

func TestTreeDir(t *testing.T) {
	out := new(bytes.Buffer)
	config := Config{
		OnlyDir:      true,
		AllFiles:     false,
		BytesSize:    false,
		HumanSize:    false,
		HumanSiSize:  false,
		WithoutColor: true,
	}

	tr := Tree{
		Output: out,
		Config: &config,
		Size:   &Size{Config: &config, Enable: IsEnabled(&config)},
	}

	err := tr.DirTree("../testdata")
	if err != nil {
		t.Errorf("test for OK Failed - error")
	}
	result := out.String()
	if result != testDirResult {
		t.Errorf("test for OK Failed - results not match\nGot:\n%v\nExpected:\n%v", result, testDirResult)
	}
}
